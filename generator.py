#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 15:24:09 2020

@author: diakhere
"""

import numpy as np
from PIL import Image 
import resize    
    
def conversion(data): 
   colours = np.zeros((16,16,3), dtype=np.uint8)
   for x in range(16): 
       for y in range(16):
           n = data[x,y]
           if int(n) == 1:
               colours[x,y] = [15,128,255]
           elif int(n) == -1:
               colours[x,y] = [254,204,102]
           else: 
                colours[x,y] = [253,102,102]
                
   img = Image.fromarray(colours) # Create a PIL image
   
   img_cropped = img.crop((4,4,12,12)) # im.crop((left, top, right, bottom))    
   return(resize.resize(img_cropped))
        
def afm(data):
    # initiate Neel state
    ref=np.ones([data.shape[0],data.shape[1]])
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            ref[i,j]=(-1)**(i+j)
    # get stagg mag
    sm = 0
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            sm=sm+(-1)**(i+j)*data[i,j]
    if sm<0:
        ref=ref*(-1)
    return ref

def deviations_array (data):
    
    ref = afm(data)
    
    deviations = np.zeros((16,16), dtype=np.uint8)
    
    for x in range(16):
        for y in range(16):
            if data[x,y] == 0:
                deviations[x,y] = 0 # holes, array do not accep negative numbers 
            elif data[x,y] == ref[x,y]:
                deviations[x,y] = 1 # similarities
            else : 
                deviations[x,y] = 2 # differences
    return deviations 



def conversion_deviations (deviations): 
    colours = np.zeros((16,16,3), dtype=np.uint8)
    for x in range(16): 
        for y in range(16):
            n = deviations[x,y]
            if int(n) == 0 :
                colours[x,y] = [253,102,102]
            elif int(n)== 2:
                colours[x,y] = [102, 153, 51]
            else:
                colours[x,y] = [102, 204, 255]
                
    img = Image.fromarray(colours) # Create a PIL image
     
    img_cropped = img.crop((4,4,12,12)) # im.crop((left, top, right, bottom))    
    return(resize.resize(img_cropped))
      
            
    

