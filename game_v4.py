#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 10:42:23 2020

@author: diakhere
"""

import sys
from PyQt5 import QtWidgets, uic, QtGui, QtCore
from PyQt5.QtWidgets import QLabel, QPushButton, QPlainTextEdit, QLineEdit
import random
import numpy as np
import csv
import os.path
import pickle 
import generator
from PIL.ImageQt import ImageQt
import threading 


""" POINT TO OTHER FILES """
""" Additional files needed """

initDialogFile = "initialization_interface.ui" # File path for initial popup dialog box
mainWindowFile = "MainWindow_interface.ui" # File path for main window GUI
    
Ui_InitDialog, QtBaseClass = uic.loadUiType(initDialogFile)
Ui_MainWindow, QtBaseClass = uic.loadUiType(mainWindowFile)

""" THE MAIN WINDOW CLASS""" 

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.setWindowTitle("Fermi-Hubbard Classification Game")
        
        self.turns = 0
        self.points = 0 
        self.indicator = 0 # 0 refers to piflux, 1 refers to strings, 2 refers to random
        self.indicator_comp = (0,1) # when 2 pictures
        self.numbers_used = []
        self.mode = "undefined"
        self.doping = 0 # 4.5 easy, 9.0 hard 
        self.level = "undefined"
        self.phase = "undefined" #training or evaluation 
        self.aspect = "raw" # raw data or deviations
        self.filename = "undefined"
        self.colors_available = ["rgb(15,128,255)", "rgb(254,204,102)" , "rgb(253,102,102)"]
        
         # [x,y,z] for (x,y) the coordinates and z the color.
        self.buttons = {self.button_0_0: [0,0,0], self.button_0_1: [0,1,1], self.button_0_2: [0,2,0], self.button_0_3: [0,3,1],
                        self.button_0_4: [0,4,0], self.button_0_5: [0,5,1], self.button_1_0: [1,0,1], self.button_1_1: [1,1,0], 
                        self.button_1_2: [1,2,1], self.button_1_3: [1,3,0], self.button_1_4: [1,4,1], self.button_1_5: [1,5,0],
                        self.button_2_0: [2,0,0], self.button_2_1: [2,1,1], self.button_2_2: [2,2,0], self.button_2_3: [2,3,1], 
                        self.button_2_4: [2,4,0], self.button_2_5: [2,5,1], self.button_3_0: [3,0,1], self.button_3_1: [3,1,0],
                        self.button_3_2: [3,2,1], self.button_3_3: [3,3,0], self.button_3_4: [3,4,1], self.button_3_5: [3,5,0],
                        self.button_4_0: [4,0,0], self.button_4_1: [4,1,1], self.button_4_2: [4,2,0], self.button_4_3: [4,3,1], 
                        self.button_4_4: [4,4,0], self.button_4_5: [4,5,1], self.button_5_0: [5,0,1], self.button_5_1: [5,1,0], 
                        self.button_5_2: [5,2,1], self.button_5_3: [5,3,0], self.button_5_4: [5,4,1], self.button_5_5: [5,5,0]}
        
        self.buttons_for_reset = {self.button_0_0: [0,0,0], self.button_0_1: [0,1,1], self.button_0_2: [0,2,0], self.button_0_3: [0,3,1],
                        self.button_0_4: [0,4,0], self.button_0_5: [0,5,1], self.button_1_0: [1,0,1], self.button_1_1: [1,1,0], 
                        self.button_1_2: [1,2,1], self.button_1_3: [1,3,0], self.button_1_4: [1,4,1], self.button_1_5: [1,5,0],
                        self.button_2_0: [2,0,0], self.button_2_1: [2,1,1], self.button_2_2: [2,2,0], self.button_2_3: [2,3,1], 
                        self.button_2_4: [2,4,0], self.button_2_5: [2,5,1], self.button_3_0: [3,0,1], self.button_3_1: [3,1,0],
                        self.button_3_2: [3,2,1], self.button_3_3: [3,3,0], self.button_3_4: [3,4,1], self.button_3_5: [3,5,0],
                        self.button_4_0: [4,0,0], self.button_4_1: [4,1,1], self.button_4_2: [4,2,0], self.button_4_3: [4,3,1], 
                        self.button_4_4: [4,4,0], self.button_4_5: [4,5,1], self.button_5_0: [5,0,1], self.button_5_1: [5,1,0], 
                        self.button_5_2: [5,2,1], self.button_5_3: [5,3,0], self.button_5_4: [5,4,1], self.button_5_5: [5,5,0]}

        """Create and Display the widgets needed"""
        
        self.user = QLabel(self)
        self.user.resize(400,350)
        self.user.move(220, 130)
        self.user.setStyleSheet("""font-size: 24px;background: rgb(51, 102, 153); border:1px solid black;""")
        
        self.name_label = QLabel("Name",self)
        self.name_label.move(380,150)
        self.name_label.setStyleSheet("""font-size: 24px;""")
        
        self.name_input = QLineEdit(self)
        self.name_input.move(253,210)
        self.name_input.resize(320,40)
        self.name_input.setStyleSheet("""font-size: 18px;background: white""")
        
        self.physicist_label = QLabel("Are you a physicist ?", self)
        self.physicist_label.move(310,280)
        self.physicist_label.resize(250,40)
        self.physicist_label.setStyleSheet("""font-size: 24px;""")
        
        self.physicist_input = QLineEdit(self)
        self.physicist_input.move(253,350)
        self.physicist_input.resize(320,40)
        self.physicist_input.setStyleSheet("""font-size: 18px;background: white""")
        
        self.end_user = QPushButton("OK",self)
        self.end_user.move(500,430)
        self.end_user.setStyleSheet("""font-size: 18px;background: white""")
        
        self.score = QLabel(self)
        self.score.move(0, 25) 
        self.score.setStyleSheet("""font-size: 15px;""")
        
        self.turns_display= QLabel("Turn: " + str(self.turns),self) 
        self.turns_display.move(0, 0) 
        self.turns_display.setStyleSheet("""font-size: 15px;""")
        
        self.turns_left_display= QLabel(self) 
        self.turns_left_display.move(0, 50) 
        self.turns_left_display.setStyleSheet("""font-size: 15px;""")
        
        self.correct = QLabel("Correct! ", self)
        self.correct.resize(200,72)
        self.correct.move(300, 435) 
        self.correct.setAlignment(QtCore.Qt.AlignCenter)
        self.correct.setStyleSheet("""font-size: 24px;background: rgb(64, 128, 2);""")
        
        self.wrong = QLabel("Wrong... ", self)
        self.wrong.resize(200,72)
        self.wrong.move(300, 435) 
        self.wrong.setAlignment(QtCore.Qt.AlignCenter)
        self.wrong.setStyleSheet("""font-size: 24px;background: rgb(204, 0, 0);""")
        
        self.easy_level = QPushButton("Easy", self)
        self.easy_level.resize(220,80)
        self.easy_level.move(300, 150)  
        self.easy_level.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.hard_level = QPushButton("Hard", self)
        self.hard_level.resize(220,80)
        self.hard_level.move(300, 400)  
        self.hard_level.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.choose_mode = QLabel("Choose your mode : ",self)
        self.choose_mode.resize(400,80)
        self.choose_mode.move(290, 0)  
        self.choose_mode.setStyleSheet("""font-size: 28px;""")
        
        self.classic_mode = QPushButton("Classic Mode", self)
        self.classic_mode.resize(220,80)
        self.classic_mode.move(10, 150)  
        self.classic_mode.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.altern_mode = QPushButton("Alternative Mode", self)
        self.altern_mode.resize(220,80)
        self.altern_mode.move(290, 150)  
        self.altern_mode.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.random_mode = QPushButton("Random Mode", self) #maybe random is misleading 
        self.random_mode.resize(220,80)
        self.random_mode.move(570, 150)  
        self.random_mode.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.comparison_mode = QPushButton("Comparison Mode", self)
        self.comparison_mode.resize(220,80)
        self.comparison_mode.move(10, 400)  
        self.comparison_mode.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.heisenberg_mode = QPushButton("Heisenberg Mode", self)
        self.heisenberg_mode.resize(220,80)
        self.heisenberg_mode.move(290, 400)  
        self.heisenberg_mode.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.training = QPushButton("See examples", self)
        self.training.resize(220,80)
        self.training.move(570, 400)  
        self.training.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.piflux_buttn = QPushButton("Pi-Flux", self)
        self.piflux_buttn.resize(200,70)
        self.piflux_buttn.move(50, 435)  
        self.piflux_buttn.setStyleSheet("""font-size: 24px;background: white;""")

        self.strings_buttn = QPushButton("Strings", self)
        self.strings_buttn.resize(200,72)
        self.strings_buttn.move(550, 435) 
        self.strings_buttn.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.random_buttn = QPushButton("Random", self)
        self.random_buttn.resize(200,72)
        self.random_buttn.move(300, 435) 
        self.random_buttn.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.piflux_buttn_trying = QPushButton("Pi-Flux", self)
        self.piflux_buttn_trying.resize(150,70)
        self.piflux_buttn_trying.move(5, 435)  
        self.piflux_buttn_trying.setStyleSheet("""font-size: 24px;background: white;""")

        self.strings_buttn_trying = QPushButton("Strings", self)
        self.strings_buttn_trying.resize(150,72)
        self.strings_buttn_trying.move(325, 435) 
        self.strings_buttn_trying.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.random_buttn_trying = QPushButton("Random", self)
        self.random_buttn_trying.resize(150,72)
        self.random_buttn_trying.move(165, 435) 
        self.random_buttn_trying.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.t1_trying = QPushButton("T = 0.1 J ", self)
        self.t1_trying.resize(150,72)
        self.t1_trying.move(485, 435) 
        self.t1_trying.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.t8_trying = QPushButton("T = 0.8 J ", self)
        self.t8_trying.resize(150,72)
        self.t8_trying.move(645, 435) 
        self.t8_trying.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.piflux_strings = QPushButton("Pi-flux  | Strings ", self)
        self.piflux_strings.resize(220,72)
        self.piflux_strings.move(295, 325) 
        self.piflux_strings.setStyleSheet("""font-size: 28px;background: white;""")
        
        self.strings_piflux = QPushButton("Strings | Pi-flux ", self)
        self.strings_piflux.resize(220,72)
        self.strings_piflux.move(295, 415) 
        self.strings_piflux.setStyleSheet("""font-size: 28px;background: white;""")
        
        self.piflux_strings_trying = QPushButton("Pi-flux  | Strings ", self)
        self.piflux_strings_trying.resize(220,72)
        self.piflux_strings_trying.move(295, 325) 
        self.piflux_strings_trying.setStyleSheet("""font-size: 28px;background: white;""")
        
        self.strings_piflux_trying = QPushButton("Strings | Pi-flux ", self)
        self.strings_piflux_trying.resize(220,72)
        self.strings_piflux_trying.move(295, 415) 
        self.strings_piflux_trying.setStyleSheet("""font-size: 28px;background: white;""")
        
        self.evaluation = QPushButton("Evaluation", self)
        self.evaluation.resize(220,80)
        self.evaluation.move(300, 400)  
        self.evaluation.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.examples = QPushButton("See examples", self)
        self.examples.resize(220,80)
        self.examples.move(300, 100)  
        self.examples.setStyleSheet("""font-size: 24px;background: white;""")

        self.escape = QPushButton("Stop the training", self) 
        self.escape.resize(220,80)
        self.escape.move(300, 400)  
        self.escape.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.go_back = QPushButton("Quit", self) 
        self.go_back.resize(200,30)
        self.go_back.move(300, 530)  
        self.go_back.setStyleSheet("""font-size: 24px;background: white;""")
   
        self.end = QPushButton("End", self) 
        self.end.resize(200,20)
        self.end.move(300, 530)  
        self.end.setStyleSheet("""font-size: 18px;background: white;""")
        
        self.strategies = QPushButton("Strategies ", self) 
        self.strategies.resize(200,20)
        self.strategies.move(300, 555)  
        self.strategies.setStyleSheet("""font-size: 18px;background: white;""")
        
        self.stop_training = QPushButton("Stop the training",self) 
        self.stop_training.setStyleSheet("""font-size: 18px;background: white;""")
        
        self.strategies_layout = QLabel(self)
        self.strategies_layout.resize(400,550)
        self.strategies_layout.move(200, 20)  
        self.strategies_layout.setStyleSheet("""font-size: 24px;background: rgb(51, 102, 153); border:1px solid black;""")
      
        self.strategies_label = QLabel("Strategies", self)
        self.strategies_label.resize(100,50)
        self.strategies_label.move(350, 30)  
        self.strategies_label.setAlignment(QtCore.Qt.AlignCenter)
        self.strategies_label.setStyleSheet("""font-size: 21px;background: white;""")
        
        self.strategies_input = QPlainTextEdit(self)
        self.strategies_input.resize(300,420)
        self.strategies_input.move(250, 95)  
        self.strategies_input.setStyleSheet("""font-size: 18px;background: white;""")
        
        self.strategies_finish = QPushButton("Finish", self)
        self.strategies_finish.resize(60,30)
        self.strategies_finish.move(520, 525)  
        self.strategies_finish.setStyleSheet("""font-size: 18px;background: white;""")
        
        self.change_aspect = QPushButton("Deviations",self)
        self.change_aspect.setCheckable(True)
        self.change_aspect.resize(80,40)
        self.change_aspect.move(715, 5)  
        self.change_aspect.setStyleSheet("""font-size: 15px;background: white;""")
        
        self.stop_training_popup = QLabel("End of the training!",self)
        self.stop_training_popup.resize(450,150)
        self.stop_training_popup.move(170,150)
        self.stop_training_popup.setStyleSheet("""font-size: 20px; border:1px solid black;""")
        self.stop_training_popup.setAlignment(QtCore.Qt.AlignCenter)
        
        self.draw_piflux_popup = QLabel("Try to draw your own Pi-flux picture!", self)
        self.draw_piflux_popup.resize(450,150)
        self.draw_piflux_popup.move(170,150)
        self.draw_piflux_popup.setStyleSheet("""font-size: 20px; border:1px solid black;""")
        self.draw_piflux_popup.setAlignment(QtCore.Qt.AlignCenter)
        
        self.draw_strings_popup = QLabel("Good job ! Now, try to create strings pictures !", self)
        self.draw_strings_popup.resize(450,150)
        self.draw_strings_popup.move(170,150)
        self.draw_strings_popup.setStyleSheet("""font-size: 20px; border:1px solid black;""")
        self.draw_strings_popup.setAlignment(QtCore.Qt.AlignCenter)
        
        self.see_examples_popup = QLabel("Click on the category that you want to see. ", self)
        self.see_examples_popup.resize(450,150)
        self.see_examples_popup.move(170,150)
        self.see_examples_popup.setStyleSheet("""font-size: 20px; border:1px solid black;""")
        self.see_examples_popup.setAlignment(QtCore.Qt.AlignCenter)
        
        self.end_heisenberg_popup = QLabel("Game over!", self)
        self.end_heisenberg_popup.resize(450,150)
        self.end_heisenberg_popup.move(170,150)
        self.end_heisenberg_popup.setStyleSheet("""font-size: 20px; border:1px solid black;""")
        self.end_heisenberg_popup.setAlignment(QtCore.Qt.AlignCenter)
        
        
        self.picture = QLabel(self) # Picture for classic, random and alternative modes
        self.picture_left = QLabel(self) # Picture for comparison mode
        self.picture_right = QLabel(self) # Picture for comparison mode
        self.picture_heisenberg = QLabel(self) # Picture for Heisenberg mode
        
        self.piflux.move(300,500) 
        self.strings.move(300,500)
        
        self.centralwidget.hide()
        
        self.piflux_buttn_trying.hide()
        self.strings_buttn_trying.hide()
        self.random_buttn_trying.hide()
        self.piflux_strings_trying.hide()
        self.strings_piflux_trying.hide()
        self.t1_trying.hide()
        self.t8_trying.hide()
        
        self.piflux_buttn.hide()
        self.strings_buttn.hide()
        self.random_buttn.hide()
        self.piflux_strings.hide()
        self.strings_piflux.hide()
        
        self.piflux.hide()
        self.strings.hide()

        self.correct.hide()
        self.wrong.hide()
        
        self.picture.hide()
        self.picture_right.hide()
        self.picture_left.hide()
        self.picture_heisenberg.hide()
        
        self.turns_display.hide()
        self.turns_left_display.hide()
        self.score.hide()
        
        self.classic_mode.hide()
        self.altern_mode.hide()
        self.random_mode.hide()
        self.comparison_mode.hide()
        self.heisenberg_mode.hide()
        self.choose_mode.hide()
        
        self.training.hide()
        self.evaluation.hide()
        self.examples.hide()
        
        self.next.hide() 
        self.go_back.hide()
        self.escape.hide()
        
        self.end.hide()
        self.strategies.hide()
        self.stop_training.hide()
        
        self.strategies_layout.hide()
        self.strategies_label.hide()
        self.strategies_input.hide()
        self.strategies_finish.hide()
        
        self.stop_training_popup.hide()
        self.draw_piflux_popup.hide()
        self.draw_strings_popup.hide() 
        self.see_examples_popup.hide()
        self.end_heisenberg_popup.hide()
        
        self.change_aspect.hide()
        
        self.user.raise_()
        self.name_label.raise_()
        self.name_input.raise_()
        self.physicist_label.raise_()
        self.physicist_input.raise_()
        self.end_user.raise_()
     
        

        """ Dynamics"""
        self.end_user.clicked.connect(self.login)
        
        self.easy_level.clicked.connect(self.easy)
        self.hard_level.clicked.connect(self.hard)
        
        self.classic_mode.clicked.connect(self.classic)
        self.altern_mode.clicked.connect(self.alternative)
        self.random_mode.clicked.connect(self.rand)
        self.comparison_mode.clicked.connect(self.comparison)
        self.heisenberg_mode.clicked.connect(self.heisenberg)
        
        self.end.clicked.connect(self.end_function)
        self.strategies.clicked.connect(self.write_strategies)
        
        self.training.clicked.connect(self.see_examples)
        self.stop_training.clicked.connect(self.stop_training_function)
        
        self.change_aspect.clicked.connect(self.change_aspect_function)
        
        self.button_0_0.clicked.connect(lambda : self.color(self.button_0_0, self.buttons[self.button_0_0]))
        self.button_0_1.clicked.connect(lambda : self.color(self.button_0_1, self.buttons[self.button_0_1])) 
        self.button_0_2.clicked.connect(lambda : self.color(self.button_0_2, self.buttons[self.button_0_2]))
        self.button_0_3.clicked.connect(lambda : self.color(self.button_0_3, self.buttons[self.button_0_3]))
        self.button_0_4.clicked.connect(lambda : self.color(self.button_0_4, self.buttons[self.button_0_4]))
        self.button_0_5.clicked.connect(lambda : self.color(self.button_0_5, self.buttons[self.button_0_5]))
        self.button_1_0.clicked.connect(lambda : self.color(self.button_1_0, self.buttons[self.button_1_0]))
        self.button_1_1.clicked.connect(lambda : self.color(self.button_1_1, self.buttons[self.button_1_1]))
        self.button_1_2.clicked.connect(lambda : self.color(self.button_1_2, self.buttons[self.button_1_2]))
        self.button_1_3.clicked.connect(lambda : self.color(self.button_1_3, self.buttons[self.button_1_3]))
        self.button_1_4.clicked.connect(lambda : self.color(self.button_1_4, self.buttons[self.button_1_4]))
        self.button_1_5.clicked.connect(lambda : self.color(self.button_1_5, self.buttons[self.button_1_5]))
        self.button_2_0.clicked.connect(lambda : self.color(self.button_2_0, self.buttons[self.button_2_0]))
        self.button_2_1.clicked.connect(lambda : self.color(self.button_2_1, self.buttons[self.button_2_1]))        
        self.button_2_2.clicked.connect(lambda : self.color(self.button_2_2, self.buttons[self.button_2_2]))
        self.button_2_3.clicked.connect(lambda : self.color(self.button_2_3, self.buttons[self.button_2_3]))
        self.button_2_4.clicked.connect(lambda : self.color(self.button_2_4, self.buttons[self.button_2_4]))
        self.button_2_5.clicked.connect(lambda : self.color(self.button_2_5, self.buttons[self.button_2_5]))
        self.button_3_0.clicked.connect(lambda : self.color(self.button_3_0, self.buttons[self.button_3_0]))
        self.button_3_1.clicked.connect(lambda : self.color(self.button_3_1, self.buttons[self.button_3_1]))
        self.button_3_2.clicked.connect(lambda : self.color(self.button_3_2, self.buttons[self.button_3_2]))
        self.button_3_3.clicked.connect(lambda : self.color(self.button_3_3, self.buttons[self.button_3_3]))
        self.button_3_4.clicked.connect(lambda : self.color(self.button_3_4, self.buttons[self.button_3_4]))
        self.button_3_5.clicked.connect(lambda : self.color(self.button_3_5, self.buttons[self.button_3_5]))
        self.button_4_0.clicked.connect(lambda : self.color(self.button_4_0, self.buttons[self.button_4_0]))
        self.button_4_1.clicked.connect(lambda : self.color(self.button_4_1, self.buttons[self.button_4_1]))
        self.button_4_2.clicked.connect(lambda : self.color(self.button_4_2, self.buttons[self.button_4_2]))
        self.button_4_3.clicked.connect(lambda : self.color(self.button_4_3, self.buttons[self.button_4_3]))
        self.button_4_4.clicked.connect(lambda : self.color(self.button_4_4, self.buttons[self.button_4_4]))
        self.button_4_5.clicked.connect(lambda : self.color(self.button_4_5, self.buttons[self.button_4_5]))
        self.button_5_0.clicked.connect(lambda : self.color(self.button_5_0, self.buttons[self.button_5_0]))
        self.button_5_1.clicked.connect(lambda : self.color(self.button_5_1, self.buttons[self.button_5_1]))
        self.button_5_2.clicked.connect(lambda : self.color(self.button_5_2, self.buttons[self.button_5_2]))
        self.button_5_3.clicked.connect(lambda : self.color(self.button_5_3, self.buttons[self.button_5_3]))
        self.button_5_4.clicked.connect(lambda : self.color(self.button_5_4, self.buttons[self.button_5_4]))
        self.button_5_5.clicked.connect(lambda : self.color(self.button_5_5, self.buttons[self.button_5_5]))
    
    """ Login information """
    
    def login(self):
        
        self.name = self.name_input.text()
        self.physicist = self.physicist_input.text()
        
        self.user.hide()
        
        self.name_label.hide()
        self.name_input.hide()
        
        self.physicist_label.hide()
        self.physicist_input.hide()
        
        self.end_user.hide()
        
        
    """ Choose level """

    def easy(self):
        self.doping = 4.5
        self.level = "easy"

        self.easy_level.hide()
        self.hard_level.hide()
        
        self.classic_mode.show()
        self.altern_mode.show()
        self.comparison_mode.show()
        self.heisenberg_mode.show()
        self.choose_mode.show()
        self.training.show()
        
        self.comparison_mode.move(570,150)
        self.heisenberg_mode.move(150,400)
        self.training.move(450,400)
        
    def hard(self):
        self.doping = 9.0
        self.level = "hard"
        
        self.easy_level.hide()
        self.hard_level.hide()
        
        self.classic_mode.show()
        self.altern_mode.show()
        self.random_mode.show()
        self.comparison_mode.show()
        self.heisenberg_mode.show()
        self.choose_mode.show()
        self.training.show()
        
        
    """ Launch modes """
    
    def classic(self):  
        
        self.mode = "classic"
        
        self.classic_mode.hide()
        self.altern_mode.hide()
        self.random_mode.hide()
        self.comparison_mode.hide()
        self.heisenberg_mode.hide()
        self.training.hide()
        self.choose_mode.hide()
        
        self.change_aspect.show()
        
        self.evaluation_function()
        
    def alternative(self):
        
        self.mode = "alternative"
        
        self.classic_mode.hide()
        self.altern_mode.hide()
        self.random_mode.hide()
        self.comparison_mode.hide()
        self.heisenberg_mode.hide()
        self.training.hide()
        self.choose_mode.hide()
        
        self.change_aspect.show()
        
        self.evaluation_function()

        
    def rand(self):
        
        self.mode = "random"
        
        self.classic_mode.hide()
        self.altern_mode.hide()
        self.random_mode.hide()
        self.comparison_mode.hide()
        self.heisenberg_mode.hide()
        self.training.hide()
        self.choose_mode.hide()
        
        self.change_aspect.show()
        
        self.correct.move(300,515)
        self.wrong.move(300,515)
        
        
        self.evaluation_function()
   
    def comparison(self):
        
        self.mode = "comparison"
         
        self.classic_mode.hide()
        self.altern_mode.hide()
        self.random_mode.hide()
        self.comparison_mode.hide()
        self.heisenberg_mode.hide()
        self.training.hide()
        self.choose_mode.hide()
        
        self.change_aspect.show()

        self.correct.resize(220,72)
        self.correct.move(295, 500)
        self.wrong.resize(220, 72)
        self.wrong.move(295,500)

        self.evaluation_function()
        
        
    def heisenberg(self):
        
        self.mode = "heisenberg"
         
        self.classic_mode.hide()
        self.altern_mode.hide()
        self.random_mode.hide()
        self.comparison_mode.hide()
        self.heisenberg_mode.hide()
        self.training.hide()
        self.choose_mode.hide()
        
        self.change_aspect.show()

        self.piflux_buttn.setText("T = 0.1 J")
        self.strings_buttn.setText("T = 0.8 J")
        
        self.piflux_buttn_trying.setText("T = 0.1 J")
        self.strings_buttn_trying.setText("T = 0.8 J")
        
        self.evaluation_function()

        
    """ Evaluation function """
        
    def evaluation_function(self):
        
        self.phase = "training"

        select = random.randint(0,1) # 0 : starts with a pi-flux picture, 1 : starts with strings 
        
        """ Generates picture"""
        
        """ Picture for classic, random and alternative modes """
        if self.mode in ["classic", "random", "alternative"] :
            self.indicator = select
            qimage = ImageQt(self.picture_generator())
            pixmap = QtGui.QPixmap.fromImage(qimage)
            self.picture.setPixmap(pixmap)
            self.picture.setScaledContents(True)
            self.picture.setGeometry(QtCore.QRect(100, 15, 600, 400))
            
        """Pictures for comparison mode"""
        if select == 0 :
           self.indicator_comp = (0,1)
        else : 
           self.indicator_comp = (1,0) 
        
        
        if self.mode == "comparison" :
            
            qimage_right = ImageQt(self.picture_generator()[0])
            qimage_left = ImageQt(self.picture_generator()[1])
        
        
            pixmap_right = QtGui.QPixmap.fromImage(qimage_right)
            self.picture_left.setPixmap(pixmap_right)
            self.picture_left.setScaledContents(True)
            self.picture_left.setGeometry(QtCore.QRect(15, 50, 380, 253))
            
            pixmap_left = QtGui.QPixmap.fromImage(qimage_left)
            self.picture_right.setPixmap(pixmap_left)
            self.picture_right.setScaledContents(True)
            self.picture_right.setGeometry(QtCore.QRect(405, 50, 380, 253))
            
        
        """Pictures for Heisenberg mode""" 
        if self.mode == "heisenberg" :   
              self.indicator = select
              qimage = ImageQt(self.picture_generator())
              pixmap = QtGui.QPixmap.fromImage(qimage)
              self.picture_heisenberg.setPixmap(pixmap)
              self.picture_heisenberg.setScaledContents(True)
              self.picture_heisenberg.setGeometry(QtCore.QRect(100, 15, 600, 400))
            
        self.evaluation.hide()
        self.training.hide()
        
        if self.mode == "classic" :
            
            self.picture.show()
            self.piflux_buttn.show()
            self.strings_buttn.show()
            self.end.show()
            self.strategies.show()
     
            self.stop_training.show()
            self.stop_training.resize(200,20)
            self.stop_training.move(300, 580)  
            self.stop_training.setStyleSheet("""font-size: 18px;background: white;""")
            
            self.piflux_buttn.clicked.connect(self.check_piflux_classic)
            self.strings_buttn.clicked.connect(self.check_strings_classic)
            
        elif self.mode == "alternative" :
            
            self.picture.show()
            self.piflux_buttn.show()
            self.strings_buttn.show()
            self.end.show()
            self.strategies.show()
     
            self.stop_training.show()
            self.stop_training.resize(200,20)
            self.stop_training.move(300, 580)  
            self.stop_training.setStyleSheet("""font-size: 18px;background: white;""")
            
            self.piflux_buttn.clicked.connect(self.check_piflux_altern)
            self.strings_buttn.clicked.connect(self.check_strings_altern)
            
        
        elif self.mode == "random" :
            
            self.picture.show()
            self.piflux_buttn.show()
            self.strings_buttn.show()
            self.random_buttn.show()
            self.end.show()
            self.strategies.show()
            
            
            self.stop_training.show()
            self.stop_training.resize(200,20)
            self.stop_training.setStyleSheet("""font-size: 18px;background: white;""")
        
            self.strategies.move(590, 515)
            self.end.move(590, 565)
            self.stop_training.move(590, 540)

            
            self.piflux_buttn.clicked.connect(self.check_piflux_random)
            self.strings_buttn.clicked.connect(self.check_strings_random)
            self.random_buttn.clicked.connect(self.check_random)
            
        elif self.mode == "comparison" :
            
            self.picture_right.show()
            self.picture_left.show()
            self.strings_piflux.show()
            self.piflux_strings.show()
            
            self.end.show()
            self.end.resize(200,30)
            self.end.move(590, 520)
            self.end.setStyleSheet("""font-size: 24px;background: white;""")
            
            self.strategies.show()
            self.strategies.resize(200,30)
            self.strategies.move(590, 555)
            self.strategies.setStyleSheet("""font-size: 24px;background: white;""")
            
            self.stop_training.show()
            self.stop_training.resize(200,30)
            self.stop_training.move(5, 530)
            self.stop_training.setStyleSheet("""font-size: 24px;background: white;""")

            self.piflux_strings.clicked.connect(self.check_strings_piflux)
            self.strings_piflux.clicked.connect(self.check_piflux_strings)
            
        else : 
            
            self.picture_heisenberg.show()
            self.piflux_buttn.show()
            self.strings_buttn.show()
            
            self.end.show()
            self.strategies.show()

            self.stop_training.show()
            self.stop_training.resize(200,20)
            self.stop_training.move(300, 580)
            self.stop_training.setStyleSheet("""font-size: 18px;background: white;""")
        

            self.piflux_buttn.clicked.connect(self.check_t1)
            self.strings_buttn.clicked.connect(self.check_t8)
            
    """Stop training """
    
    def stop_training_function(self):
        
        self.phase = "evaluation"
        
        self.filename = results_file(self.mode,self.level,self.name,self.physicist)
        
        self.correct.hide()
        self.wrong.hide()
        
        self.turns = 0
        self.points = 0 
        
        self.stop_training.hide()
        self.stop_training_popup.show()
        self.stop_training_popup.raise_()
        timer = threading.Timer(1.0, self.stop_training_popup.hide) 
        timer.start() 
        
        if self.mode == "random":
            self.strategies.resize(200,30)
            self.end.resize(200,30)
            self.end.move(590,550)
            
        elif self.mode == "alternative":

            self.picture.hide()  
            self.piflux_buttn.hide()
            self.strings_buttn.hide()
            self.end.hide()
            self.strategies.hide()
            
            self.centralwidget.show()
            self.piflux.show()
            self.next.show()
            self.draw_piflux_popup.show()
            self.draw_piflux_popup.raise_()
            
            timer = threading.Timer(1.0, self.draw_piflux_popup.hide) 
            timer.start() 
            
            self.next.clicked.connect(self.reset) # in the alternative mode
        
            
    """ Quit training """    
    def go_back_function(self): 
        
        self.correct.hide()
        self.wrong.hide()
        
        self.picture.hide()
        self.picture_right.hide()
        self.picture_left.hide()
        self.picture_heisenberg.hide()

        
        self.piflux_buttn_trying.hide()
        self.strings_buttn_trying.hide()
        self.random_buttn_trying.hide()
        self.piflux_strings_trying.hide()
        self.strings_piflux_trying.hide()
        self.t1_trying.hide()
        self.t8_trying.hide()
        
        self.go_back.hide()
        self.stop_training.hide()
        
        self.change_aspect.hide()
            
        self.classic_mode.show()
        self.altern_mode.show()
        self.random_mode.show()
        self.comparison_mode.show()
        self.heisenberg_mode.show()
        self.training.show()
        self.choose_mode.show()
        
        
       

    """ Training : see examples """
        
    def see_examples(self):
        
        self.phase = "training"
        
        self.change_aspect.show()
        
        self.see_examples_popup.show()
        self.see_examples_popup.raise_()
        timer = threading.Timer(1.0, self.see_examples_popup.hide) 
        timer.start() 
            
        self.classic_mode.hide()
        self.altern_mode.hide()
        self.random_mode.hide()
        self.comparison_mode.hide()
        self.heisenberg_mode.hide()
        self.training.hide()
        self.choose_mode.hide()
        
        
        self.piflux_buttn_trying.show()
        self.strings_buttn_trying.show()
        self.random_buttn_trying.show()
        self.t1_trying.show()
        self.t8_trying.show()


        self.piflux_buttn_trying.clicked.connect(self.show_piflux)
        self.strings_buttn_trying.clicked.connect(self.show_strings)
        self.random_buttn_trying.clicked.connect(self.show_random)
        self.t1_trying.clicked.connect(self.show_t1)
        self.t8_trying.clicked.connect(self.show_t8)
        
        self.go_back.move(590,560)
            
        if self.level == "easy":
            
            self.random_buttn_trying.hide()
            
            self.piflux_buttn_trying.move(90,435)
            self.strings_buttn_trying.move(250,435)
            self.t1_trying.move(410,435)
            self.t8_trying.move(570,435)
            
            self.go_back.move(310, 530) 
                

    
        self.go_back.show()
        self.go_back.clicked.connect(self.go_back_function)
         
        
    def show_piflux(self):
        
        self.mode = "random"
        
        self.indicator = 0 
        
        qimage = ImageQt(self.picture_generator())    
        new_pixmap = QtGui.QPixmap.fromImage(qimage)
        self.picture.setPixmap(new_pixmap)
        self.picture.setScaledContents(True)
        self.picture.setGeometry(QtCore.QRect(100, 15, 600, 400))
        
        self.picture.show()
        self.picture.raise_()
        
    def show_strings(self):
        
        self.mode = "random"

        self.indicator = 1
    
        qimage = ImageQt(self.picture_generator())
        new_pixmap = QtGui.QPixmap.fromImage(qimage)
        self.picture.setPixmap(new_pixmap)
        self.picture.setScaledContents(True)
        self.picture.setGeometry(QtCore.QRect(100, 15, 600, 400))
        
        self.picture.show()
        self.picture.raise_()
        
    def show_random(self):
        
        self.mode = "random"
        
        self.indicator = 2
        
        qimage = ImageQt(self.picture_generator())
        new_pixmap = QtGui.QPixmap.fromImage(qimage)
        self.picture.setPixmap(new_pixmap)
        self.picture.setScaledContents(True)
        self.picture.setGeometry(QtCore.QRect(100, 15, 600, 400))
        
        self.picture.show()
        self.picture.raise_()
        
    def show_t1(self):
        
        self.mode = "heisenberg" 
        
        self.indicator = 0
        
        qimage = ImageQt(self.picture_generator())
        pixmap = QtGui.QPixmap.fromImage(qimage)
        self.picture_heisenberg.setPixmap(pixmap)
        self.picture_heisenberg.setScaledContents(True)
        self.picture_heisenberg.setGeometry(QtCore.QRect(100, 15, 600, 400))
        
        self.picture_heisenberg.show()
        self.picture_heisenberg.raise_()
        
    def show_t8(self):
        
        self.mode = "heisenberg"
        
        self.indicator = 1
        
        qimage = ImageQt(self.picture_generator())
        pixmap = QtGui.QPixmap.fromImage(qimage)
        self.picture_heisenberg.setPixmap(pixmap)
        self.picture_heisenberg.setScaledContents(True)
        self.picture_heisenberg.setGeometry(QtCore.QRect(100, 15, 600, 400))
    
        
        self.picture_heisenberg.show()
        self.picture_heisenberg.raise_()
  
        
    """ Check functions """
    
    def check_piflux_classic(self):
        
        self.correct.hide()
        self.wrong.hide()

        if self.indicator == 0 : 
            self.correct.show()
            self.points += 1
            results = ['Pi-flux',  1]        
        else : 
            self.wrong.show()
            results = ['Strings', 0]
        append_results(self.phase, self.filename, results)
        self.nextTurn_classic()
        
    def check_strings_classic(self):
        
        self.correct.hide()
        self.wrong.hide() 
        
        if self.indicator == 1 : 
            self.correct.show()
            self.points += 1
            results = ['Strings', 1]
        else : 
            self.wrong.show()
            results = ['Pi-flux',0]
            
        append_results(self.phase, self.filename, results)
        self.nextTurn_classic()
        
    def check_piflux_altern(self):
        
        self.correct.hide()
        self.wrong.hide()
        
        if self.indicator == 0 : 
            self.correct.show()
            self.points += 1
        else : 
            self.wrong.show()
        self.nextTurn_altern()

    def check_strings_altern(self):
        
        self.correct.hide()
        self.wrong.hide()
        
        if self.indicator == 1 : 
            self.correct.show()
            self.points += 1
        else : 
            self.wrong.show()
        self.nextTurn_altern()
        
    def check_piflux_random(self):
        
        self.correct.hide()
        self.wrong.hide()
        
        self.piflux_buttn.setStyleSheet("""font-size: 24px;background: white;""")
        self.piflux_buttn_trying.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.strings_buttn.setStyleSheet("""font-size: 24px;background: white;""")
        self.strings_buttn_trying.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.random_buttn.setStyleSheet("""font-size: 24px;background: white;""")
        self.random_buttn_trying.setStyleSheet("""font-size: 24px;background: white;""")

        if self.indicator == 0 : 
            self.correct.show()
            self.points += 1
            results = ['Pi-flux', 1]
        else : 
            self.wrong.show()
            if self.indicator == 1:
                self.strings_buttn.setStyleSheet("""font-size: 24px;background: green;""")
                self.strings_buttn_trying.setStyleSheet("""font-size: 24px;background: green;""")
                results = ['Strings', 0]
            else:
                self.random_buttn.setStyleSheet("""font-size: 24px;background: green;""")
                self.random_buttn_trying.setStyleSheet("""font-size: 24px;background: green;""")
                results = ['Random', 0]
        
        append_results(self.phase, self.filename, results)
        self.nextTurn_random()

    def check_strings_random(self):
        
        self.correct.hide()
        self.wrong.hide()
                
        self.piflux_buttn.setStyleSheet("""font-size: 24px;background: white;""")
        self.piflux_buttn_trying.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.strings_buttn.setStyleSheet("""font-size: 24px;background: white;""")
        self.strings_buttn_trying.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.random_buttn.setStyleSheet("""font-size: 24px;background: white;""")
        self.random_buttn_trying.setStyleSheet("""font-size: 24px;background: white;""")
        
        if self.indicator == 1 : 
            self.correct.show()
            self.points += 1
            results = [ 'Strings', 1]
        else : 
            self.wrong.show()
            if self.indicator == 0:
                self.piflux_buttn.setStyleSheet("""font-size: 24px;background: green;""")
                self.piflux_buttn_trying.setStyleSheet("""font-size: 24px;background: green;""")
                
                results = ['Pi-flux', 0]
            else:
                self.random_buttn.setStyleSheet("""font-size: 24px;background: green;""")
                self.random_buttn_trying.setStyleSheet("""font-size: 24px;background: green;""")
                results = ['Random',  0]
            
        append_results(self.phase, self.filename, results)
        self.nextTurn_random()
        
    def check_random(self):
        
        self.correct.hide()
        self.wrong.hide()
        
        self.piflux_buttn.setStyleSheet("""font-size: 24px;background: white;""")
        self.piflux_buttn_trying.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.strings_buttn.setStyleSheet("""font-size: 24px;background: white;""")
        self.strings_buttn_trying.setStyleSheet("""font-size: 24px;background: white;""")
        
        self.random_buttn.setStyleSheet("""font-size: 24px;background: white;""")
        self.random_buttn_trying.setStyleSheet("""font-size: 24px;background: white;""")
        
        if self.indicator == 2 : 
            self.correct.show()
            self.points += 1
            results = ['Random', 1]
        else : 
            self.wrong.show()
            if self.indicator == 0:
                self.piflux_buttn.setStyleSheet("""font-size: 24px;background: green;""")
                self.piflux_buttn_trying.setStyleSheet("""font-size: 24px;background: green;""")
                results = ['Pi-flux', 0]
            else:
                self.strings_buttn.setStyleSheet("""font-size: 24px;background: green;""")
                self.strings_buttn_trying.setStyleSheet("""font-size: 24px;background: green;""")
                results = ['Strings', 0]
            
        append_results(self.phase, self.filename, results)
        self.nextTurn_random()
        
    def check_piflux_strings(self):
        
        self.correct.hide()
        self.wrong.hide()
        
        if self.indicator_comp == (0,1) : 
            self.correct.show()
            self.points += 1
            results = ['NULL' , 1]
        else : 
            self.wrong.show()
            results = ['NULL' , 0]
            
        append_results(self.phase, self.filename, results)
        self.nextTurn_comp()

    def check_strings_piflux(self):
        
        self.correct.hide()
        self.wrong.hide()
        
        if self.indicator_comp == (1,0) : 
            self.correct.show()
            self.points += 1
            results = [ 'NULL' , 1]
        else :
            self.wrong.show()
            results = ['NULL', 0]
        
        append_results(self.phase, self.filename, results)
        self.nextTurn_comp()
        
    def check_t1(self):
        
        self.correct.hide()
        self.wrong.hide()
        
        if self.indicator == 0 : 
            self.correct.show()
            self.points += 1
            results = ['T = 0.1 J', 1]
        else : 
            self.wrong.show()
            results = ['T = 0.8 J', 0]
            
        append_results(self.phase, self.filename, results)
        self.nextTurn_heisenberg()
        
    def check_t8(self):
        
        self.correct.hide()
        self.wrong.hide()
        
        if self.indicator == 1 : 
            self.correct.show()
            self.points += 1
            results = ['T = 0.8 J',  1]
        else : 
            self.wrong.show()
            results = ['T = 0.1 J', 0]
            
        append_results(self.phase, self.filename, results)
        self.nextTurn_heisenberg()
    
    """Next turn functions """
    
    def nextTurn_classic(self):
        
        if self.phase == "evaluation" : 
            self.turns += 1
            self.turns_display.setText("Turn : " + str(self.turns))
            self.score.setText("Points : " + str(self.points))
            self.score.move(0,22)
            
            self.score.show()
            self.turns_display.show()
    
        self.indicator = random.randint(0,1)
        
        qimage = ImageQt(self.picture_generator())
        new_pixmap = QtGui.QPixmap.fromImage(qimage)
        self.picture.setPixmap(new_pixmap)
        self.picture.setScaledContents(True)
        self.picture.setGeometry(QtCore.QRect(100, 15, 600, 400))
        
    def nextTurn_altern(self):
        
        self.indicator = random.randint(0,1)
        
        qimage = ImageQt(self.picture_generator())
        new_pixmap = QtGui.QPixmap.fromImage(qimage)
        self.picture.setPixmap(new_pixmap)
        self.picture.setScaledContents(True)
        self.picture.setGeometry(QtCore.QRect(100, 15, 600, 400))
          
    def nextTurn_random(self):
        
        if self.phase == "evaluation" : 
            self.turns += 1
            self.turns_display.setText("Turn : " + str(self.turns))
            self.score.setText("Points : " + str(self.points))
            self.score.move(0,22)
            
            self.score.show()
            self.turns_display.show()
            
        self.indicator = random.randint(0,2)
        
        qimage = ImageQt(self.picture_generator())
        new_pixmap = QtGui.QPixmap.fromImage(qimage)
        self.picture.setPixmap(new_pixmap)
        self.picture.setScaledContents(True)
        self.picture.setGeometry(QtCore.QRect(100, 15, 600, 400))
        
    def nextTurn_comp(self):
        
        if self.phase == "evaluation" : 
            self.turns += 1
            self.turns_display.setText("Turn : " + str(self.turns))
            self.score.setText("Points : " + str(self.points))
            self.score.move(0,22)
            
            self.score.show()
            self.turns_display.show()
            
            
        select = random.randint(0,1)
        
        if select == 0 :
           self.indicator_comp = (0,1)
        else : 
           self.indicator_comp = (1,0) 
           
        qimage_left = ImageQt(self.picture_generator()[0])
        qimage_right = ImageQt(self.picture_generator()[1])
        
        pixmap_left = QtGui.QPixmap.fromImage(qimage_left)
        self.picture_left.setPixmap(pixmap_left)
        self.picture_left.setScaledContents(True)
        self.picture_left.setGeometry(QtCore.QRect(15, 50, 380, 253))
            
        pixmap_right = QtGui.QPixmap.fromImage(qimage_right)
        self.picture_right.setPixmap(pixmap_right)
        self.picture_right.setScaledContents(True)
        self.picture_right.setGeometry(QtCore.QRect(405, 50, 380, 253))
           
            
    def nextTurn_heisenberg(self):
        
       if self.phase == "evaluation" : 
            self.turns += 1
            self.turns_display.setText("Turn : " + str(self.turns))
            self.score.setText("Points : " + str(self.points))
            self.score.show()
            self.turns_display.show()
            
     
       self.indicator = random.randint(0,1)

       qimage = ImageQt(self.picture_generator())
       pixmap = QtGui.QPixmap.fromImage(qimage)
       self.picture_heisenberg.setPixmap(pixmap)
       self.picture_heisenberg.setScaledContents(True)
       self.picture_heisenberg.setGeometry(QtCore.QRect(100, 15, 600, 400))
        
    """ Alternative Complementary Functions"""
    
    def color(self,k,v):
        """Change the color when clicked. k is the button, v is the 3rd element in the list (cf self.buttons) """
        v[2] += 1
        v[2] = v[2]%3
        k.setStyleSheet("font-size: 24px;background: " + self.colors_available[v[2]] + ";")
        
    def reset(self):
        
        drawing = self.colors_to_array()
        
        
        if self.turns == 0 :
            
            results = ['Pi-flux', drawing]
            
            self.strategies.resize(120,30)
            self.end.resize(120, 30)
            
            self.strategies.move(650, 490) 
            self.end.move(650, 525)
            
            self.piflux.hide()
            self.next.hide()
            
            self.strings.show()
            self.end.show()
            self.strategies.show()
            
            self.draw_strings_popup.show()
            self.draw_strings_popup.raise_()
            timer = threading.Timer(1.0, self.draw_strings_popup.hide) 
            timer.start()
            
            for (k,v) in self.buttons.items() : 
                v[2] = self.buttons_for_reset[k][2]
                k.setStyleSheet("font-size: 24px;background: " + self.colors_available[v[2]] + ";")

            self.turns += 1
            
        elif self.turns == 1 :
            results = ['Strings', drawing]


        append_results(self.phase, self.filename, results)
        
    def reset_deviations(self):
        for (k,v) in self.buttons.items() : 
            v[2] = self.buttons_for_reset[k][2]
            k.setStyleSheet("font-size: 24px;background: " + self.colors_available[v[2]] + ";")
            

        
            
    def colors_to_array(self):
       drawing_array = np.zeros((6,6), dtype=np.uint8)
       if self.colors_available == ["rgb(15,128,255)", "rgb(254,204,102)" , "rgb(253,102,102)"] : 
           colors_available_array = [[1,15, 128, 255], [2, 254, 204, 102], [0, 253, 102, 102]]
       else : 
            colors_available_array = [[1, 102, 204, 255], [2, 102, 153, 51], [0, 253, 102, 102]]
       for v in self.buttons.values():
           v[2] = v[2]%3
           color = colors_available_array[v[2]]
           drawing_array[v[0],v[1]] = color[0]
       drawing_list = drawing_array.tolist() 
       for x in range(6) :
           for y in range(6) :
               if drawing_list[x][y] == 2 :
                  drawing_list[x][y] = -1                
       return drawing_list
   
    """ Startegies """
    
    def write_strategies(self):

        self.strategies_label.show()
        self.strategies_input.show()
        self.strategies_layout.show()
        self.strategies_finish.show()
        
        self.strategies_layout.raise_()
        self.strategies_label.raise_()
        self.strategies_input.raise_()
        self.strategies_finish.raise_()
        
        

        self.strategies_finish.clicked.connect(self.hide_strategies)
        
    
    def hide_strategies(self):
  
        self.strategies_label.hide()
        self.strategies_input.hide()
        self.strategies_layout.hide()
        self.strategies_finish.hide()
        
        if self.end == "yes" :
            get_strategies(self.mode,self.level, self.strategies_input.toPlainText(),self.name, self.physicist)
            sys.exit(app.exec_())
        
        
    """ Generate pictures """ 
    
    def picture_generator(self):
        depickled = depickling(self.level, self.mode)
        
        if self.mode in ["classic", "random", "alternative"] :
            k = random.randint(0,9999)
            if self.indicator == 0 :
                m = 0  
            else:
                m = random.randint(1,2)
            
            while (self.indicator, m, k) in self.numbers_used :
                
                k = random.randint(0,9999)
                
                if self.indicator == 0 :
                    m = 0  
                else:
                    m = random.randint(1,2)
        
            if self.indicator == 0 :
                data = depickled[self.indicator][k]
            else:
                data = depickled[self.indicator][m][k]
            
            self.numbers_used.append((self.indicator, m, k))
            
            if self.aspect == "raw":
                return(generator.conversion(data))
            else:
                return(generator.conversion_deviations(generator.deviations_array(data)))
            
        elif self.mode == "comparison":
            k0 = random.randint(0,9999)
            k1 = random.randint(0,9999)
            
            if self.indicator_comp == (0,1):
                m0 = 0 
                m1 = random.randint(1,2)
            else : 
                m0 = random.randint(1,2) 
                m1 = 0
            
            while (self.indicator_comp[0], m0, k0) in self.numbers_used or (self.indicator_comp[1], m1, k1) in self.numbers_used:
    
                k0 = random.randint(0,9999)
                k1 = random.randint(0,9999)
                
                
                if self.indicator_comp == (0,1):
                   m0 = 0 
                   m1 = random.randint(1,2)
                else : 
                   m0 = random.randint(1,2) 
                   m1 = 0
            
            if self.indicator_comp == (0,1):
                data0 = depickled[0][k0]
                data1 = depickled[1][m1][k1]
                
            else :
                data0 = depickled[1][m0][k0]
                data1 = depickled[0][k1]
                
            self.numbers_used.append((self.indicator_comp[0], m0, k0))
            self.numbers_used.append((self.indicator_comp[1], m1, k1))
            
            if self.aspect == "raw":
                return((generator.conversion(data0),generator.conversion(data1)))
            else:
                return((generator.conversion_deviations(generator.deviations_array(data0)),
                        generator.conversion_deviations(generator.deviations_array(data1))))
                
        else:
            k = random.randint(0,2499)
            m = random.randint(1,2)
            
            while (self.indicator, m, k) in self.numbers_used :
                
                k = random.randint(0,2499)
                
                m = random.randint(1,2)
            
            while (self.indicator, m, k) in self.numbers_used :
                
                k = random.randint(0,2499)
                m = random.randint(1,2)
        
            if self.indicator == 0 :
                data = depickled[self.indicator][m][k]
            else:
                data = depickled[self.indicator][m][k]
            
            self.numbers_used.append((self.indicator, m, k))
            
            if self.aspect == "raw":
                return(generator.conversion(data))
            else:
                return(generator.conversion_deviations(generator.deviations_array(data)))
        
            if self.indicator == 0 :
                data = depickled[self.indicator][k]
            else:
                data = depickled[self.indicator][m][k]
            
            self.numbers_used.append((self.indicator, m, k))
            
            if self.aspect == "raw":
                return(generator.conversion(data))
            else:
                return(generator.conversion_deviations(generator.deviations_array(data)))
            
           
    def change_aspect_function(self):
        
        depickled = depickling(self.level, self.mode)
        
        if self.mode in ["classic", "random"] :
            
            (indic, m, k) = self.numbers_used[-1]
            
            if indic == 0 :
                data = depickled[indic][k]
            else:
                data = depickled[indic][m][k]
            
            if self.aspect == "deviations" :        
                
                self.aspect = "raw"

                qimage = ImageQt(generator.conversion(data))
                new_pixmap = QtGui.QPixmap.fromImage(qimage)
                self.picture.setPixmap(new_pixmap)
                self.picture.setScaledContents(True)
                self.picture.setGeometry(QtCore.QRect(100, 15, 600, 400))
        
            else: 
                
                self.aspect = "deviations"
                
                qimage = ImageQt(generator.conversion_deviations(generator.deviations_array(data)))
                new_pixmap = QtGui.QPixmap.fromImage(qimage)
                self.picture.setPixmap(new_pixmap)
                self.picture.setScaledContents(True)
                self.picture.setGeometry(QtCore.QRect(100, 15, 600, 400))
                
        elif self.mode == "alternative":
            
            if self.phase == "training":
                
                (indic, m, k) = self.numbers_used[-1]
            
                if indic == 0 :
                    data = depickled[indic][k]
                else:
                    data = depickled[indic][m][k]
            
                if self.aspect == "deviations" :        
                
                    self.aspect = "raw"

                    qimage = ImageQt(generator.conversion(data))
                    new_pixmap = QtGui.QPixmap.fromImage(qimage)
                    self.picture.setPixmap(new_pixmap)
                    self.picture.setScaledContents(True)
                    self.picture.setGeometry(QtCore.QRect(100, 15, 600, 400))
        
                else: 
                
                    self.aspect = "deviations"
                
                    qimage = ImageQt(generator.conversion_deviations(generator.deviations_array(data)))
                    new_pixmap = QtGui.QPixmap.fromImage(qimage)
                    self.picture.setPixmap(new_pixmap)
                    self.picture.setScaledContents(True)
                    self.picture.setGeometry(QtCore.QRect(100, 15, 600, 400))
  
            else:   
                if self.aspect == "deviations" :
                    self.aspect = "raw"
                    self.colors_available = ["rgb(15,128,255)", "rgb(254,204,102)" , "rgb(253,102,102)"]
                    
                else: 
                    self.aspect = "deviations"
                    self.colors_available = ["rgb(102, 153, 51)", "rgb(102, 204, 255)" , "rgb(253,102,102)"]
                    
                self.reset_deviations()
                
                
                
        elif self.mode == "comparison" :
            
            (indic0, m0, k0) = self.numbers_used[-2]
            (indic1, m1, k1) = self.numbers_used[-1]
            
            if (indic0, indic1) == (0,1):
                data0 = depickled[0][k0]
                data1 = depickled[1][m1][k1]
                
            else :
                data0 = depickled[1][m0][k0]
                data1 = depickled[0][k1]
                
            if self.aspect == "deviations" :
                
                self.aspect = "raw"
                
                qimage_left = ImageQt(generator.conversion(data0))
                qimage_right = ImageQt(generator.conversion(data1))
        
                pixmap_left = QtGui.QPixmap.fromImage(qimage_left)
                self.picture_left.setPixmap(pixmap_left)
                self.picture_left.setScaledContents(True)
                self.picture_left.setGeometry(QtCore.QRect(15, 50, 380, 253))
            
                pixmap_right = QtGui.QPixmap.fromImage(qimage_right)
                self.picture_right.setPixmap(pixmap_right)
                self.picture_right.setScaledContents(True)
                self.picture_right.setGeometry(QtCore.QRect(405, 50, 380, 253))
        
            else: 
                
                self.aspect = "deviations"
                
                qimage_left = ImageQt(generator.conversion_deviations(generator.deviations_array(data0)))
                qimage_right = ImageQt(generator.conversion_deviations(generator.deviations_array(data1)))
        
                pixmap_left = QtGui.QPixmap.fromImage(qimage_left)
                self.picture_left.setPixmap(pixmap_left)
                self.picture_left.setScaledContents(True)
                self.picture_left.setGeometry(QtCore.QRect(15, 50, 380, 253))
            
                pixmap_right = QtGui.QPixmap.fromImage(qimage_right)
                self.picture_right.setPixmap(pixmap_right)
                self.picture_right.setScaledContents(True)
                self.picture_right.setGeometry(QtCore.QRect(405, 50, 380, 253))
                
        else: 
            
            (indic, m, k) = self.numbers_used[-1]
            
            data = depickled[indic][m][k]
            
            if self.aspect == "deviations" :        
                
                self.aspect = "raw"

                qimage = ImageQt(generator.conversion(data))
                new_pixmap = QtGui.QPixmap.fromImage(qimage)
                self.picture_heisenberg.setPixmap(new_pixmap)
                self.picture_heisenberg.setScaledContents(True)
                self.picture_heisenberg.setGeometry(QtCore.QRect(100, 15, 600, 400))
        
            else: 
                
                self.aspect = "deviations"
                
                qimage = ImageQt(generator.conversion_deviations(generator.deviations_array(data)))
                new_pixmap = QtGui.QPixmap.fromImage(qimage)
                self.picture_heisenberg.setPixmap(new_pixmap)
                self.picture_heisenberg.setScaledContents(True)
                self.picture_heisenberg.setGeometry(QtCore.QRect(100, 15, 600, 400))
            
            
    """ Game Over """
    
    def end_function(self):
    
        if self.mode == "alternative": 
            self.reset()
            ret = QtWidgets.QMessageBox.information(self, "End", "Good job! Thank you! Do you want to share your startegies?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        else : 
            if self.turns == 0 :
                grade = 0 
            else :
                grade = self.points/self.turns
            ret = QtWidgets.QMessageBox.information(self, "End", "Good job! Thank you! Your probablity of success has reached: "
                                          + str(grade) + ". Do you want to share your strategies?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        if ret == QtWidgets.QMessageBox.No:
            get_strategies(self.mode,self.level, self.strategies_input.toPlainText(),self.name, self.physicist)
            sys.exit(app.exec_())
        else :
            self.end = "yes"
            self.write_strategies()

""" STORAGE """
            
def get_strategies(mode,level,strategies_input,name,physicist):
    i = 0 
    filename = "./Results/" + str(i)+ '_strategies_'+ mode + '_' + level  + '_' + name + '_' + physicist + '.txt'
    
    while os.path.isfile(filename) :
        i += 1
        filename = "./Results/" + str(i)+ '_strategies_'+ mode +  '_' + level  + '_' + name + '_' + physicist + '.txt'
        
    f = open(filename, mode='w+')
    f.write(strategies_input)
    f.close()
    
def results_file(mode,level,name,physicist):
    i = 0 
    filename = "./Results/ "+ str(i)+ '_results_'+ mode + '_' + level + '_' + name + '_' + physicist + '.csv'
    
    while os.path.isfile(filename) :
        i += 1
        filename = "./Results/" + str(i)+ '_results_'+ mode +  '_' + level  + '_' + name + '_' + physicist +'.csv'
        
    with open(filename, mode='w+') as results_file:
        fieldnames = ['Category', 'Result']
        writer = csv.DictWriter(results_file, fieldnames=fieldnames)
                                
        writer.writeheader()
        
    return filename

def append_results(phase, filename, results):
   if  phase == "evaluation":
       with open(filename, 'a+', newline='') as write_obj:
           csv_writer = csv.writer(write_obj)
           csv_writer.writerow(results)
           
           
    
  

""" DEPICKLING """
def depickling (level, mode): 
    if mode != "heisenberg":
        if level == "easy":
            filename_0 = "fermisea_d4.5_fullinfo.pickle" #pi-flux # 19500, 16, 16
            filename_1 = "AS_T0.6_d4.5_nodh_fullinfo.pkl" # strings # 3, 10000, 16, 16
        
            files = [filename_0, filename_1]
        else:   
            filename_0 = "fermisea_d9.0_fullinfo.pickle" #pi-flux # 19500, 16, 16
            filename_1 = "AS_T0.6_d9.0_nodh_fullinfo.pkl" # strings # 3, 10000 (except for 0th),16,16
            filename_2 = "sprinkling_d9.0_nodh_fullinfo.pkl" # sprinkled holes # 3, 10000, 16, 16
            files = [filename_0, filename_1, filename_2]
            
    else:            
        filename_0 = "QMC_T0.1.pkl" # T = 0.1 # 3, 2500 (except for the 0th), 16, 16
        filename_1 = "QMC_T0.8.pkl" # T =0.8 # 3, 2500 (except for the 0th),16,16 
        files = [filename_0, filename_1]
    
    depickled = []
    for file in files:   
        infile = open(file,'rb')
        depickled.append(pickle.load(infile, encoding='bytes'))
        infile.close()
    return depickled



""" INITIALIZATION """            
class InitDialog(QtWidgets.QDialog, Ui_InitDialog):
    def __init__(self):
        super().__init__()
        QtWidgets.QDialog.__init__(self)        
        Ui_InitDialog.__init__(self)
        self.setupUi(self)
        self.setWindowTitle("Welcome!")
        self.Ok_Button.clicked.connect(self.close)
        self.textBrowser.setOpenExternalLinks(True) # Allow to open external link 
        

""" MAIN CALL """
if __name__ == "__main__":
    
    app = QtWidgets.QApplication(sys.argv)

    window = MainWindow()
    window.show()
    window_init = InitDialog()
    window_init.exec()
    window_init.activateWindow()
    sys.exit(app.exec_())