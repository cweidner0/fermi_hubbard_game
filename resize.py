#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 14:54:56 2020

@author: diakhere
"""

from PIL import Image

def resize (img_cropped): 
    new_image = img_cropped.resize((604, 604))
    white_back = Image.open('./white_background.png')
    final_img = white_back.copy()
    final_img.paste(new_image, (313,96))
    return final_img    
            
